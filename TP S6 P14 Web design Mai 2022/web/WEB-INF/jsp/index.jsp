<%@page import="java.util.List"%>
<%@page import="model.Categorie"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html>
<html lang="en">
    <%@ include file = "head.jsp" %>
    <body class="bg-gradient-primary">
        <div id="wrapper">
            <%@ include file = "navbar.jsp" %>
            <div class="row justify-content-center" style="margin-top: -10px">
                <div class="row" style="height: 300px; background-color: #fff;">
                    <center>
                        <h1 style="margin-top: 50px; color: #4E73DF ">AI Information</h1>
                        <br>
                        <p>"À Poudlard, Harry se fait rapidement deux amis, Ron Weasley et Hermione Granger,
                            et il découvre un monde magique incroyable, rempli de sorts, de potions et de créatures fantastiques.
                            Il apprend également que ses parents ont été assassinés par le mage noir Voldemort."</p>
                    </center>
                </div>
                <div class="row">
                    <center>
                        <h2 style="margin-top: 20px">Populaire en ce moment</h2>
                    </center>
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-4">
                            <div class="card special-skill-item border-0">
                                 <center>
                                <div style="margin-top: 20px;width: 350px;height: 250px;background-color:#DFE4E6">
                                    
                                </div>   
                                    <div class="card-body">
                                        <h4 class="card-title">ChatGPT</h4>
                                        <p class="card-text">À Poudlard, Harry se fait rapidement deux amis, Ron Weasley et Hermione Granger</p>
                                    </div>
                                </center>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card special-skill-item border-0">
                                 <center>
                                <div style="margin-top: 20px;width: 350px;height: 250px;background-color:#DFE4E6">
                                    
                                </div>   
                                    <div class="card-body">
                                        <h4 class="card-title">Bard Google</h4>
                                        <p class="card-text">À Poudlard, Harry se fait rapidement deux amis, Ron Weasley et Hermione Granger</p>
                                    </div>
                                </center>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card special-skill-item border-0">
                                <center>
                                <div style="margin-top: 20px;width: 350px;height: 250px;background-color:#DFE4E6">
                                    
                                </div>                     
                                    <div class="card-body">
                                        <h4 class="card-title">Bing AI</h4>
                                        <p class="card-text">À Poudlard, Harry se fait rapidement deux amis, Ron Weasley et Hermione Granger</p>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>    
                <div class="row" style="margin-top: 50px;height: 500px; background-color: #fff;">
                    <center>
                        <h1 style="margin-top: 20px; color: #4E73DF ">About Us</h1>
                    </center>
                </div>
            </div>
            <%@ include file = "footer.jsp" %>
    </body>
</html>
