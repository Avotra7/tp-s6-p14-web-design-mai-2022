<%-- 
    Document   : list
    Created on : 6 mai 2023, 10:08:46
    Author     : user
--%>

<%@page import="java.util.List"%>
<%@page import="model.V_List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<V_List> list = (List<V_List>) request.getAttribute("list");
%>
<!DOCTYPE html>
<html lang="en">
    <%@ include file = "head.jsp" %>
    <body class="bg-gradient-primary">
        <div id="wrapper">
            <%@ include file = "navbar.jsp" %>
            <div class="row justify-content-center" style="margin-top: -10px">
                <div class="row">
                    <center>
                        <h2 style="margin-top: 20px">Tout les AI populaire</h2>
                    </center>
                    <div class="row" style="margin-top: 15px;">
                        <%
                            for (V_List vlist : list) {
                        %>
                        <div class="col-md-4" >
                            <div class="card special-skill-item border-0">
                                <center>
                                    <div style="margin-top: 20px;width: 350px;height: 250px;">
                                        <% if(vlist.getPhoto() == null){ %>
                                        <img style="width: 250px;height: 250px;border: none" src="resources/assets/img/chatgpt.jpg">
                                        <% } else{ %>
                                        <img style="width: 250px;height: 250px;border: none" src="resources/assets/img/<% out.print(vlist.getPhoto()); %>">
                                        <% } %>
                                    </div>   
                                    <div class="card-body">
                                        <a href="https://<% out.print(vlist.getLien()); %>"><h4 class="card-title"><% out.print(vlist.getIntitule()); %></h4></a>
                                        <h6><% out.print(vlist.getSociete()); %></h6>
                                        <p class="card-text"><% out.print(vlist.getDescription()); %></p>
                                    </div>
                                </center>
                                        <p style="color: #858796"> Categorie : <% out.print(vlist.getCategorie()); %></p>
                            </div>
                        </div>
                        <% } %>
                    </div> 
                </div>
            </div>
                <%@ include file = "footer.jsp" %>
                </body>
                </html>
