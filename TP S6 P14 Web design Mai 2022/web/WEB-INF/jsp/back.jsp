<%@page import="java.util.List"%>
<%@page import="model.Categorie"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<Categorie> list = (List<Categorie>) request.getAttribute("test");
%>
<!DOCTYPE html>
<html>
    <%@ include file = "head.jsp" %>
    <body class="bg-gradient-primary">
        <div id="wrapper">
            <%@ include file = "navbar_1.jsp" %>
            <div class="card shadow" style="width:800px; margin-left: 250px">
                <div class="card-header py-3">
                    <p class="text-primary m-0 fw-bold">Insertion nouveau article</p>
                </div>
                <div class="card-body">
                    <form action="save" method="post">
                        <div class="row">
                            <div class="col">
                                <div class="mb-3"><label class="form-label" for="Intitule"><strong>Intitule</strong></label><input class="form-control" type="text" placeholder="Intitule" name="Intitule"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3"><label class="form-label" for="categorie"><strong>Categorie</strong></label>
                                    <select class="form-select" name="categorie">
                                        <% for (Categorie cat : list) { %>
                                        <option value="<% out.print(cat.getId()); %>"><% out.print(cat.getIntitule()); %></option>
                                        <% } %>
                                    </select></div>
                            </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3"><label class="form-label" for="societe"><strong>societe</strong></label><input class="form-control" type="text" placeholder="societe" name="societe"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3"><label class="form-label" for="lien"><strong>Lien du site</strong></label><input class="form-control" type="text" placeholder="www.....com" name="lien"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3"><label class="form-label" for="payant"><strong>Payant</strong></label>
                                    <select class="form-select" name="payant">
                                        <option value="true">oui</option>
                                        <option value="false">non</option>
                                    </select></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3"><label class="form-label" for="description"><strong>description</strong></label><textarea class="form-control" id="editor" name="editor"></textarea></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3"><label class="form-label" for="photo"><strong>upload photo</strong></label><input class="form-control" type="file" name="photo"></div>
                            </div>
                        </div>
                        <div class="mb-3"><button class="btn btn-primary btn-sm" type="submit">Enregistrer</button></div>
                    </form>
                </div>
            </div>
        </div>
        <script>
            ClassicEditor
                    .create(document.querySelector('#editor'), {

                    })
                    .then(editor => {
                        window.editor = editor;
                    })
                    .catch(err => {
                        console.error(err.stack);
                    });
        </script>
        <%@ include file = "footer.jsp" %>
    </body>
</html>


