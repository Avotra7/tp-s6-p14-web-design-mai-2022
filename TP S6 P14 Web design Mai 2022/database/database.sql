create database webDesign;

\c webdesign;

create table categorie(
    id serial primary key,
    intitule varchar(20)
);

create table intelart(
    id serial primary key,
    intitule varchar(50),
    idCategorie int,
    description text,
    societe varchar(50),
    isPayant boolean,
    lien text,
    photo text,
    foreign key(idCategorie) references categorie(id)
);

CREATE TABLE users (
  id serial primary key,
  username varchar(50) NOT NULL,
  password varchar(50) NOT NULL,
  email varchar(50) NOT NULL
);

insert into categorie values (default,'Image'),(default,'text'),(default,'vocal'),(default,'video'),(default,'analyse'),(default,'Autre');


insert into intelart values (default,'ChatGPT',2,'modèle de langage créé par OpenAI basé sur l architecture GPT (Generative Pre-trained Transformer) pour répondre à des questions, générer du texte et accomplir d autres tâches de traitement de langage nature','OpenAI',true,'Chat.openai.com','');
insert into intelart values (default,'ImageCreator',1,'application web basée sur l IA qui permet de créer des images à partir de textes','BingAI',true,'BingImageCreator.com','');

create view v_list as
select intelart.id,intelart.intitule,categorie.intitule as categorie,intelart. description,intelart.societe,intelart.isPayant,intelart.lien,intelart.photo
from intelart join categorie on intelart.idCategorie = categorie.id;

