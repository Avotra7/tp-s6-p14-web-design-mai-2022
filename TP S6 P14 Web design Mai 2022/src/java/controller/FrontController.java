/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateDAO;
import javax.servlet.http.HttpServletRequest;
import model.Categorie;
import model.V_List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author user
 */
@Controller
public class FrontController {
    @Autowired
    HibernateDAO dao;
    
    @RequestMapping("/")
    public String home(Model model,HttpServletRequest request){
//        model.addAttribute("test", dao.findAll(Categorie.class));
        return "index";
    }
    @RequestMapping("/list")
    public String list(Model model,HttpServletRequest request){
        model.addAttribute("list", dao.findAll(V_List.class));
        return "list";
    }
}
