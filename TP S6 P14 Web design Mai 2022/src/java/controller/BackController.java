/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateDAO;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.Categorie;
import model.Intelart;
import model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author user
 */
@Controller
public class BackController {
     @Autowired
    HibernateDAO dao;
     
     @RequestMapping("/admin")
    public String admin(Model model,HttpServletRequest request){
        model.addAttribute("test", dao.findAll(Categorie.class));
        return "back";
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(Model model, HttpServletRequest request){
        Intelart art = new Intelart();
        art.setIntitule(request.getParameter("Intitule"));
        art.setSociete(request.getParameter("societe"));
        art.setIdCategorie(Integer.parseInt(request.getParameter("categorie")));
        art.setLien(request.getParameter("lien"));
        art.setIsPayant(Boolean.valueOf(request.getParameter("payant")));
        art.setDescription(request.getParameter("editor"));
        art.setPhoto(request.getParameter("photo"));
        dao.create(art);
        return "redirect:";
    }
    public List<Users> login(Users u){
        return dao.findWhere(u);
    }
    
    public static void main(String[] args) {
        Users u = new Users("admin", "admin", "admin@gmail.com");
        BackController b = new BackController();
        List<Users> us = b.login(u);
        for (Users u1 : us) {
            System.out.println(u1.getUsername());
        }
    }
}
