/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.persistence.Entity;

/**
 *
 * @author user
 */
@Entity
public class Intelart extends BaseModel{
    private String intitule;
    private int idCategorie;
    private String description;
    private String societe;
    private boolean isPayant;
    private String lien;
    private String photo;

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public int getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(int idCategorie) {
        this.idCategorie = idCategorie;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSociete() {
        return societe;
    }

    public void setSociete(String societe) {
        this.societe = societe;
    }

    public boolean isIsPayant() {
        return isPayant;
    }

    public void setIsPayant(boolean isPayant) {
        this.isPayant = isPayant;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Intelart(String intitule, int idCategorie, String description, String societe, boolean isPayant, String lien, String photo) {
        this.intitule = intitule;
        this.idCategorie = idCategorie;
        this.description = description;
        this.societe = societe;
        this.isPayant = isPayant;
        this.lien = lien;
        this.photo = photo;
    }

    public Intelart() {
    }
}
